Integrantes:
    -Sofía Carrasco     Rol: 201521061-6
    -Claudio Vergara    Rol: 201573564-6

Instrucciones:

-Se asume que el usuario tiene instalado Python 3.8.x.
-Abrir dos terminales en carpeta que contiene los archivos "cliente.py" y "server.py"
-En una terminal, digitar: 
    >py server.py
    o
    >python3 server.py
-En la otra terminal, digitar:
    >py cliente.py
    o
    >python3 cliente.py

-Escribir URL cuando lo solicite la terminal.
-Para detener la ejecución del programa, escribir "terminate" en la terminal de cliente, cuando solicite la URL.

Se asume que:
-El cliente solicita URL's validas.
-Al digitar terminate, tanto servidor, como cliente terminan su ejecución.

Información adicional:
-Los archivos de texto que contienen los headers, se guardaran en la misma carpeta en donde se abra la terminal.
