# TCPServer
import socket as s

# cache: arreglo de tupla (query, head) ordenado por uso de mayor a menor (el de más a la derecha es el menos usado)


def en_cache(array, query):
    for i in array:
        if i[0] == query:
          #  array.insert(0,i)
          #  array.pop(5)
            array.insert(0, array.pop(array.index(i)))
            return i[1]  # retorna head de la query
    return False


def agregar_cache(array, query, head):
    array.insert(0, ((query, head)))
    if len(array) > 5:
        array.pop(5)
    return

    # revisar si la query existe en arreglo cache
    # si está, moverlo a la primera posición
    # si no está y cachesize < 5
    # agregar al final del arreglo y +=1 a cachesize
    # si no está y cachesize >= 5
    # reemplazar el elemento al principio del arreglo
    # toda la maniobra para buscar el menos usado


cache = []
serverPortX = 55558


# Conexion entre cliente y server
socketServidor = s.socket(s.AF_INET, s.SOCK_STREAM)  # TCP
socketServidor.bind(('', serverPortX))

# conexion entre server y http (internet)

socketServidor.listen(5)  # Encola 5 peticiones

while True:  # siempre abierto el server
    # handshake entre cliente-server
    socketCliente, direccionCliente = socketServidor.accept()
    # handshake entre server y http

    mensajeUrl = socketCliente.recv(2048).decode()

    if mensajeUrl == "terminate":
        break

    respuesta_cache = en_cache(cache, mensajeUrl)

    if respuesta_cache:
        respuesta = respuesta_cache

    else:
        socketHTTPServidor = s.socket(s.AF_INET, s.SOCK_STREAM)  # TCP
        # conexion entre socket y web
        socketHTTPServidor.connect((mensajeUrl, 80))

        print("Se recibio la URL: ", mensajeUrl)
        # Esta es la función que saca un URL, y se saca el header a lo prah
        queryEncode = "GET / HTTP/1.1\r\n\r\n"
        socketHTTPServidor.send(queryEncode.encode())

        # ciclo para sacar las cosas de socketHTTP

        buf = socketHTTPServidor.recv(900)
        socketHTTPServidor.close()

        if not buf:
            continue
        else:
            # decodifica la linea de texto, para convertirla a char
            buf = buf.decode()
            # limpiamos y asignamos buf, solo a la primera linea del header.
            try:
                #buf, _ = buf.split('\n\n', 1)
                agregar_cache(cache, mensajeUrl, buf)
                respuesta = buf
            except:
                agregar_cache(cache, mensajeUrl, buf)
                respuesta = buf

            # ponele play
            #agregar_cache(cache, mensajeUrl, buf)
            #respuesta = buf

    socketCliente.send(respuesta.encode())

    serverPortZ = 55559
    socketServidorUDP = s.socket(s.AF_INET, s.SOCK_DGRAM)
    socketServidorUDP.bind(('', serverPortZ))
    while True:
        mensaje, direccionCliente = socketServidorUDP.recvfrom(2048)
        if mensaje.decode() == "OK":
            socketServidorUDP.sendto(respuesta.encode(), direccionCliente)
            break

socketServidorUDP.close()
socketCliente.close()
socketServidor.close()
