# UDPclient

import socket as s

direccionServidor = 'localhost'
serverPort = 55555

socketCliente = s.socket(s.AF_INET, s.SOCK_DGRAM)

mensajeAEnviar = input("Ingresar texto : ")
socketCliente.sendto(mensajeAEnviar.encode(),
                     (direccionServidor, serverPort))

mensajeAEnviar, _ = socketCliente.recvfrom(2048)
print(mensajeAEnviar.decode())
socketCliente.close()
