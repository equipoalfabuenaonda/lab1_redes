# UDPserver
import socket as s
serverPort = 55555

socketServidor = s.socket(s.AF_INET, s.SOCK_DGRAM)

socketServidor.bind(('', serverPort))

while True:
    mensaje, direccionCliente = socketServidor.recvfrom(2048)

    decodificado = mensaje.decode()

    print("Se recibio: ", decodificado)
    respuesta = "Respuesta: " + decodificado.upper()

    socketServidor.sendto(respuesta.encode(), direccionCliente)
