# TCPclient

import socket as s
import os


def limpiaHeader(header):
    for i in range(len(header)):
        if header[i] == ('<' or '\n\n'):
            head = header[: i-1]
            return head
    return header


direccionServidor = 'localhost'
serverPortX = 55558

# socketCliente = s.socket(s.AF_INET, s.SOCK_STREAM)  # IPv4 TCP
# socketCliente.connect((direccionServidor, serverPortX))  # Handshake


while True:

    socketCliente = s.socket(s.AF_INET, s.SOCK_STREAM)  # IPv4 TCP
    socketCliente.connect((direccionServidor, serverPortX))  # Handshake

    msjeAEnviar = input("URL de página : ")

    # enviamos mensaje codificado a servidor
    socketCliente.send(msjeAEnviar.encode())

    if msjeAEnviar == 'terminate':
        socketCliente.close()
        break

    # Asumiendo que llega respuesta del server

    respuesta = socketCliente.recv(2048).decode()

    socketCliente.close()

    ############### FIN TCP ######################

    serverPortZ = 55559

    socketClienteUDP = s.socket(s.AF_INET, s.SOCK_DGRAM)

    confirmo = "OK"
    socketClienteUDP.sendto(confirmo.encode(),
                            (direccionServidor, serverPortZ))

    Headertxt, _ = socketClienteUDP.recvfrom(2048)
    Headertxt = Headertxt.decode()
    socketClienteUDP.close()

    head = limpiaHeader(Headertxt)

    try:
        archivoURL = open("./" + msjeAEnviar + ".txt", 'x')
        archivoURL.write(head + "\n")
        archivoURL.close()
    except:
        continue
