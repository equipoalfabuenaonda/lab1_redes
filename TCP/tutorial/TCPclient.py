# TCPclient

import socket as s

direccionServidor = 'localhost'
serverPort = 55556

socketCliente = s.socket(s.AF_INET, s.SOCK_STREAM)
socketCliente.connect((direccionServidor, serverPort))  # realiza handshake

mensajeAEnviar = input('ingrese texto: ')

socketCliente.send(mensajeAEnviar.encode())

respuesta = socketCliente.recv(2048).decode()

print(respuesta)
socketCliente.close()
