# TCPserver
# Abrir Server primero

import socket as s

serverPort = 55555

# AF_INET -> IPv4
socketServidor = s.socket(s.AF_INET, s.SOCK_STREAM)  # stream indica TCP

socketServidor.bind(('', serverPort))

# que sea 1, indica que solo un elemento puede estar en la cola
socketServidor.listen(1)  # socket server solo handshake

while True:
    # socketcliente exclusivo para comunicacion
    socketCliente, direccionCliente = socketServidor.accept()
    mensaje = socketCliente.recv(2048).decode()
    print("Se recibio: ", mensaje)
    respuesta = "Respuesta: " + mensaje.upper()
    socketCliente.send(respuesta.encode())
    socketCliente.close()

